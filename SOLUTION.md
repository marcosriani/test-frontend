# SOLUTION

Tests -
Test if what is returned from the API is an object.
Test if my components are functions.
Test if the application is rendering
Test the stats
Test the events

## Estimation

Estimated: 1:30 hours

Spent: 2:30 hours

## Solution

Comments on your solution

I could write better styles for the page, also better functionality to filter the data where one filter impacts the other. I could also improve the performance of the application by reducing repetition or hard coded values. Could add a functionality to fetch more data like a next button. I could truncate the text to show less. I could add ... for when the data is loading. I could create pages for each item that is returned, and finally I could write tests for each component, and remove all files that are not used.
