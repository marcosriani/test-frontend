import React from 'react';

import classes from './Products.module.css';

const Products = (props) => {
  return (
    <div>
      {props.products.map((product) => {
        return (
          <div key={product.id} className={classes.product__detail}>
            <p className={classes.product__name}>{product.name}</p>
            <p className={classes.product__body}>{product.body}</p>
            <p className={classes.product__other}>{product.email}</p>
          </div>
        );
      })}

      <span className={classes.product__search__n}>
        Your search returned {props.returnOfSearch} product
      </span>
    </div>
  );
};

export default Products;
