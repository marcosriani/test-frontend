import React from 'react';

import classes from './Filters.module.css';

const Filters = (props) => {
  return (
    <div className={classes.filters}>
      <label htmlFor="filters" className={classes.filters__label}>
        Filter products:
      </label>

      <select
        id="filters2"
        onChange={(e) => props.handleClick(e.target.value)}
        className={classes.filters__selector}
      >
        <option value={'12'}>Subscriber</option>
        <option value={'12'}>Yes</option>
      </select>

      <select
        id="filters"
        onChange={(e) => props.handleClick(e.target.value)}
        className={classes.filters__selector}
      >
        <option value={'12'}>All</option>
        <option value={'10'}>Dog</option>
        <option value={'1'}>Price (30)</option>
        <option value={'4'}>Cat</option>
      </select>
    </div>
  );
};

export default Filters;
