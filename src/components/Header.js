import React from 'react';

import classes from './Header.module.css';

const Header = () => {
  return (
    <div className={classes.products__wrapper}>
      <img src="/logo.png" alt="logo" />
      <h1 className={classes.products}>My Products</h1>
    </div>
  );
};

export default Header;
