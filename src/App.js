import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Header from './components/Header';
import Products from './components/Products';
import Filters from './components/Filters';

const App = () => {
  const [data, setData] = useState([]);
  const [limitSearchTerm, setLimitSearchTerm] = useState(12);

  const handleClick = (optionClicked) => {
    setLimitSearchTerm(optionClicked);
  };

  useEffect(() => {
    const fetchProducts = async () => {
      const result = await axios(
        `https://jsonplaceholder.typicode.com/comments?_page=1&_limit=${limitSearchTerm}`
      );

      setData(result.data);
    };

    fetchProducts();
  }, [limitSearchTerm, setLimitSearchTerm]);

  return (
    <div>
      <Header />
      <Filters handleClick={handleClick} />
      <Products
        products={data}
        handleClick={handleClick}
        returnOfSearch={limitSearchTerm}
      />
    </div>
  );
};

export default App;
